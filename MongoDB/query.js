var http = require('http');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
var i = 0;

//find records

/*MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  db.collection("students").find({}).toArray(function(err, result) {
    if (err) throw err;
    for(; i<result.length; i++){
    	console.log(result[i].st_name);
    }
    db.close();
  });
});*/

// find using filter

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  //var query = { st_id: 1510876151 };
  /*var query = { st_name: /^A/ }
  db.collection("students").find(query).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });*/

  //sorting 
  /*var mysort = { st_name: -1 };
  db.collection("students").find().sort(mysort).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });*/

  //remove  records
  var myquery = { st_name: 'Nasim' };
  db.collection("students").remove(myquery, function(err, obj) {
    if (err) throw err;
    console.log(obj.result.n + " record(s) deleted");
    db.close();
  });


});