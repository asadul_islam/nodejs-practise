var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var myobj = [
    { st_name: 'Nasim', st_id: 1510576103},
    { st_name: 'Ashraful', st_id: 1510576106},
    { st_name: 'Ripon', st_id: 1510576107},
    { st_name: 'Shuvo', st_id: 1510676151}
  ];
  db.collection("students").insert(myobj, function(err, res) {
    if (err) throw err;
    console.log("Number of records inserted: " + res.insertedCount);
    db.close();
  });
});