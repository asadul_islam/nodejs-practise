var mysql = require('mysql');
var http = require('http');
var rs = {};
var text = "<table style = 'border: 1px; border-collapse:collapse'>";

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    port: 3306,
    password: "",
    database: "testing"
});

http.createServer(function(req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/html'
    });

    con.connect(function(err) {
        if (err) {
            console.log("Database Connection Failed")
            return;
        }
        console.log("Database Connected!");
    });

    con.query("SELECT * FROM user", function(err, rows, fields) {
        if (err) throw err;
        text += text + '<tr><th>Id</th><th>Name</th></tr>';
        for (var i = 0; i < rows.length; i++) {
            text += "<tr><td>" + rows[i]["id"] + "</td><td>" + rows[i]["name"] + '</td></tr>';
            //res.write(rows[i] + '\n');
            //rs[i] = rows[i];
            //console.log(rs[i]);
            //res.end(JSON.stringify(fields) + '\n');
        }
        text += "</table>";
        res.end(text);
        text = "";
    });

    //res.end();

    //con.end(function(err) {
    // if (err) throw err;
    //console.log('Closing Database Connection');
    //});
}).listen(3000);