var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');


var app = express();

//app.use(bodyParser);
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'));
//app.use(express.static('public'))

var studentSchema = mongoose.Schema({
  id: {type:Number, required:true, index:true, unique:true},
  name: {type:String, required:true},
  age: Number,
  address: String,
  salary: Number
})

var stu_info = mongoose.model('stu_info', studentSchema);

mongoose.connect('mongodb://localhost/mydb', function (err) {
  if (err) {
    console.log('can not connect database');
    return;
  }
  console.log('database connected');
})

/*
///// insert records
var asad = new stu_info({
  id: 1510876151,
  name: 'Asad',
  age: 21,
  address: 'Sirajganj',
  salary: 23132
});

asad.save(function (err, stu_info) {
  if (err){
    console.log('err');
  } else {
    console.log('added' + stu_info);
  }
});*/

//// get method
app.get('/', function (req, res) {
  res.writeHead(200, "type: text/html");
  res.write('./index.html');
})

app.get('/student', function (req, res) {
  res.render('student')
})


app.post('/student', function (req, res) {
  var st_info = req.body["myForm"];
  if (st_info["id"]) {
    res.render('show_message', {message: 'Sorry, you don\'t insert information correctly', type: 'error'})
  }
  else {
    var newStudent = new stu_info({
      id: st_info['id'],
      name: st_info['name'],
      age: st_info['age'],
      address: st_info['address'],
      salary: st_info['salary']
    });

    newStudent.save(function (err, st_info) {
      if (err){
        res.render('show_message', {message: "Database error", type: "error"});
      } else {
        res.render('show_message', {message: "New person added", type: "success", student: st_info});
      }
    });
  }

  console.log(req.body);

});

app.listen(3000)