var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var stu_info = require('../models/students');

//mongoose.Promise = global.Promise;
mongoose.Promise = require('bluebird');

/* GET home page. */
router.get('/', function (req, res, next) {
    mongoose.connect('mongodb://localhost/mydb', function (err) {
        if (err) {
            console.log('can not connect database');
            throw err;
        }
        console.log('database connected');
    });

    stu_info.find(function (err, rows) {
        if (err) {
            res.send('database error', {title: 'All Data'});
        }
        else {
            res.render('index', {title: 'All student information', rows: rows});
        }
    });
    mongoose.disconnect(function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('Connection Closed');
        }
    });
});

module.exports = router;
