var mongoose = require('mongoose');
var studentSchema = mongoose.Schema({
  id: {type:Number, required:true, index:true, unique:true},
  name: {type:String, required:true},
  age: Number,
  address: String,
  salary: Number
});

module.exports = mongoose.model('stu_info', studentSchema);
