var express = require('express');
var router = express.Router();

/* GET students listing. */
router.get('/students', function(req, res, next) {
  res.render('students', {
    title: 'Student Page',
    name: 'All Student Data'
  });
});

module.exports = router;
