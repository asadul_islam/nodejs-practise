var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var student = require("../models/students");

//mongoose.Promise = global.Promise;
mongoose.Promise = require("bluebird");

/* GET users listing. */
router.get("/students", function(req, res, next) {
    res.render("students", {
        title: "Student",
        name: "All Student data"
    });
});

router.get("/students/:id", function(req, res, next) {
    mongoose.connect("mongodb://localhost/mydb", function(err) {
        if (err) {
            console.log("can not connect database");
            throw err;
        }
        console.log("database connected");
    });

    student.find({ id: req.params.id }, function(err, rows) {
        if (err) {
            res.send("database error", { title: "Not Found" });
        } else {
            res.send("PPP", { title: "Found the record", rows: rows });
        }
    });
    mongoose.disconnect(function(err) {
        if (err) {
            console.log(err);
        } else {
            console.log("Connection Closed");
        }
    });
});

/*router.post('/students', function (req, res, next) {
 if (!req.body.id || !req.body.name || !req.body.age || !req.body.address || !req.body.salary) {
 res.send('Sorry, fill in the form correctly.')
 res.redirect('/students')
 } else {
 mongoose.connect('mongodb://localhost/mydb', function (err) {
 if (err) {
 console.log('can not connect database')
 throw err
 }
 console.log('database connected')
 })
 var newStudent = new stu_info({
 id: req.body.id,
 name: req.body.name,
 age: req.body.age,
 address: req.body.address,
 salary: req.body.salary
 })
 newStudent.save(function (err, result) {
 if (err) {
 res.send('database error')
 } else {
 res.send('Inserted ' + result)
 }
 })
 mongoose.disconnect(function (err) {
 if (err) {
 console.log(err)
 } else {
 console.log('Connection Closed')
 }
 })
 }
 })*/

module.exports = router;