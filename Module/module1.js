var myString  = 'Hello JavaScript';

function myFunction () {
  console.log('Module is running');
}

module.exports.myFunction = myFunction;
module.exports.myString = myString;